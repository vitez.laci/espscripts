# ESPScripts

Build, debug and monitor scripts for ESP
These script helps you to integrate QT Creattor with ESP32 family.
If you would like to use QT Creator to debug ESP32 boards via JTAG you should check my <a href="https://laszlovitez.com/2020/03/15/debug-esp32-wroom-with-esp32-prog-and-qt-creator/">blog post</a>.
