#!/bin/bash

if [ -z $1 ] || [ ! -d $1 ]
then
    echo "Invalid project dir $1"
    exit -1
fi

cd $1
. $IDF_PATH/export.sh
if [ $2 == "clean" ]
then
    idf.py clean
fi
idf.py build
