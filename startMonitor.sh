#!/bin/bash

VID_WROOM=10c4
PID_WROOM=ea60

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
. $DIR/sub_helpers.sh

if [ -z $1 ] || [ ! -d $1 ]
then
    echo "Invalid project dir $1"
    exit -1
fi

if [ ! -z "$3" ]
then
offset=$3
fi

setMountPoint $VID_WROOM $PID_WROOM $offset

if [ -z $mountPoint ]
then
    echo "No WROOM device found with offset $offset" 1>&2
    exit -1
fi

if [ ! -z "$2" ] && [ "$2" = "reflash" ]
then
reflashCmd="idf.py flash -p $mountPoint &&"
fi

cmd="cd $1 && \
    . $IDF_PATH/export.sh && \
     $reflashCmd \
    idf.py monitor -p $mountPoint"

killOld idf_monitor
gnome-terminal --window -- /bin/bash -c "$cmd"
#/bin/bash -c "$cmd"

