#!/bin/bash

VID_JTAG=0403
PID_JTAG=6010

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
. $DIR/sub_helpers.sh

setMountPoint $VID_JTAG $PID_JTAG 0
if [ -z $mountPoint ]
then
    echo "No JTAG debugger found" 1>&2
    exit -1
fi
if [ ! -z "$1" ] && [ "$1" = "NORTOS" ]
then
NORTOS="-c 'set ESP32_RTOS none'"
fi

killOld openocd
gnome-terminal --window -- /bin/bash -c ". $IDF_PATH/export.sh && openocd -f interface/ftdi/esp32_devkitj_v1.cfg $NORTOS -f board/esp-wroom-32.cfg -c 'init; reset; halt;'"


