function setMountPoint()
{
local lVID=$1
local lPID=$2
local actDir=`pwd`
local loffset=$3
cd /sys/class/tty
while read -r device
do
    local actPathBase=`readlink $device`
    local aVID=`cat "$actPathBase/../../../../idVendor"`
    if [ $aVID != $lVID ]
    then
        continue
    fi
    local aPID=`cat "$actPathBase/../../../../idProduct"`

    if [ $aPID == $lPID ]
    then
        if ((loffset>0))
        then
            ((--loffset))
            continue
        fi
        mountPoint="/dev/$device"
        break
    fi
done < <(ls | grep USB*)
cd $actDir
}

function killOld()
{
local OLD_PID=`ps axf | grep $1 | grep -v grep | awk '{print $1}'`
if [ ! -z $OLD_PID ]
then
    echo $OLD_PID | xargs kill
fi
}
